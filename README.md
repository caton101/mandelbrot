# Mandelbrot

This is my new mandelbrot renderer built using C++ and SDL2. The old one can be
found [here][0].

![Screenshot](screenshot.png)

[0]: https://gitlab.com/caton101/sdl-playground/-/tree/master/mandelbrot
