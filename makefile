CC = g++
INC = -Iinclude -Llib -Wall -lSDL2
CFLAGS = -c -std=c++20 $(INC)
OBJ = mandelbrot.o

run:
	make mandelbrot
	./mandelbrot

mandelbrot: $(OBJ)
	$(CC) $(INC) $(OBJ) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

mandelbrot.o: \
	mandelbrot.hpp \
	settings.hpp

clean:
	rm -f $(OBJ)
	rm -f mandelbrot

reset:
	make clean
	make mandelbrot
