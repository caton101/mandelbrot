// HEADER: this tells the preprocessor to only make one copy of the header
#ifndef MANDELBROT_H
#define MANDELBROT_H

// LIBRARIES: these are required for the program to run
#include <SDL2/SDL.h>
#include <noise/noise.h>
#include <iostream>
#include <chrono>
#include <stack>

// VALUES: these should never be changed
#define RENDER_MODE_RANDOM 0 // render random pixel locations to boost framerate (useful for exploration)
#define RENDER_MODE_ALL 1    // render all pixels on the screen (useful for screenshots)

// NAMESPACES: this makes things easier to type
using namespace noise;
using namespace std::chrono;

// TYPES: these are used for keeping runtime information
typedef struct
{
    double x;
    double y;
    double w;
    double h;
} Camera;

typedef struct
{
    int w;
    int h;
} WindowSize;

typedef struct
{
    int x1;
    int y1;
    int x2;
    int y2;
} MouseRect;

// DEFINITIONS: these define function names, arguments and return types
void stop(SDL_Renderer *r, SDL_Window *w);
double getTime();
void updateDeltaTime();
double mandelbrotFormula(double x, double y, int iterations);
int snapMandelbrotIterations(int value);
void drawPixelAtLocation(int x, int y, SDL_Renderer *renderer, Camera *camera, int iterations, double scaleX, double scaleY);
void render(SDL_Renderer *renderer, WindowSize *windowSize, Camera *camera, int renderMode, int mandelbrotIterations);
void camera_zoom(Camera *camera, double delta);
void camera_resize(Camera *camera, int oldW, int oldH, int newW, int newH);
int main(int argc, char **argv);

// HEADER: this tells the preprocessor where the end of the header is
#endif
