/*
 * SETTINGS
 *
 * Each setting below can be changed at will. Please ensure you respect any type
 * or value contraints mentioned for each one.
 */

/**
 * Selection Color
 *
 * The RGB color used for the selection box
 *
 * Type: uint8
 * Range: 0 to 255
 * Unit: 8-bit color space
 */
#define COLOR_SELECT_R 255
#define COLOR_SELECT_G 50
#define COLOR_SELECT_B 50

/**
 * Inside Color Bias
 *
 * The color bias used for pixels inside the mandelbrot set. This can be used to
 * "tint" the greyscale value into a specific color
 *
 * Type:  float
 * Range: 0 to 1
 * Unit:  fractional color space
 */
#define COLOR_BIAS_R 0.25
#define COLOR_BIAS_G 1
#define COLOR_BIAS_B 0.25

/**
 * Above Clip Color
 *
 * The RGB color used for values above clipping range
 *
 * Type: uint8
 * Range: 0 to 255
 * Unit: 8-bit color space
 */
#define COLOR_ABOVE_R 255
#define COLOR_ABOVE_G 0
#define COLOR_ABOVE_B 0

/**
 * Below Clip Color
 *
 * The RGB color used for values below clipping range
 *
 * Type: uint8
 * Range: 0 to 255
 * Unit: 8-bit color space
 */
#define COLOR_BELOW_R 0
#define COLOR_BELOW_G 0
#define COLOR_BELOW_B 255

/**
 * Screen Size
 *
 * The window size in pixels
 *
 * Type:  uint
 * Range: 1 to MAX_UINT
 * Unit:  Pixels
 */
#define DEFAULT_SCREEN_WIDTH 1000
#define DEFAULT_SCREEN_HEIGHT 500

/**
 * Camera Position
 *
 * The camera position (with respect to the top-left corner)
 *
 * Type:  float
 * Range: -inf to inf
 * Unit:  fractional cartesian coordinate
 */
#define DEFAULT_CAMERA_X -2.86
#define DEFAULT_CAMERA_Y -1.06

/**
 * Camera Zoom Speed
 *
 * The camera zoom speed
 *
 * Type:  float
 * Range: 0 to inf
 * Unit:  fractional cartesian coordinate units per second
 */
#define CAMERA_ZOOM_SPEED 0.3

/**
 * Camera Zoom Speed
 *
 * The camera pan (movement) speed
 *
 * Type:  float
 * Range: 0 to inf
 * Unit:  fractional cartesian coordinate units per second
 */
#define CAMERA_PAN_SPEED 0.5

/**
 * Camera Scale
 *
 * The ratio between the window size and camera size
 *
 * Type:  float
 * Range: 0 to inf
 * Unit:  percentage
 */
#define CAMERA_SCALE 0.004276

/**
 * Camera Clip Value
 *
 * The highest mandelbrot value that can be applied to the color gradient
 *
 * Type:  uint
 * Range: 0 to 255
 * Unit:  8-bit color space
 */
#define CAMERA_CLIP_VALUE 255

/**
 * Fractal Diverge Threshhold
 *
 * The largest size a mandelbrot value can be before it is considered to have
 * diverged outside the set
 *
 * Type:  float
 * Range: 0 to inf
 * Units: generic fractional value
 */
#define FRACTAL_DIVERGE_THESHHOLD 255

/**
 * Mandelbrot Min Iterations
 *
 * The smallest number of iterations used to test for divergence
 *
 * Type: uint
 * Range: 1 to MAX_UINT
 * Units: generic whole value
 */
#define MANDELBROT_MIN_ITERATIONS 100

/**
 * Mandelbrot Max Iterations
 *
 * The largest number of iterations used to test for divergence
 *
 * Type: uint
 * Range: 1 to MAX_UINT
 * Units: generic whole value
 */
#define MANDELBROT_MAX_ITERATIONS 100000

/**
 * Mandelbrot Default Iterations
 *
 * The default number of iterations used to test for divergence
 *
 * Type: uint
 * Range: 1 to MAX_UINT
 * Units: generic whole value
 */
#define MANDELBROT_DEFAULT_ITERATIONS 100

/**
 * Mandelbrot Delta Iterations
 *
 * The change in iterations used to increase/decrease iteration count
 *
 * Type: uint
 * Range: 1 to MAX_UINT
 * Units: iterations per second
 */
#define MANDELBROT_DELTA_ITERATIONS 1000

/**
 * Framerate
 *
 * The number of frames to render per second (only works if using
 * RENDER_MODE_RANDOM)
 *
 * Type:  uint
 * Range: 1 to MAX_UINT
 * Units: frames per second
 */
#define FRAMERATE 60

/**
 * Debug Mode
 *
 * Toggles debug information being printed to STDOUT
 *
 * Type:  bool
 * Range: true or false
 * Units: yes or no
 */
#define DEBUG_MODE false
